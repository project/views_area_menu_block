<?php

/**
 * Implements hook_views_data().
 */
function views_area_menu_block_views_data() {
  $data['views']['views_area_menu_block'] = [
    'title' => t('Views Area Menu block'),
    'help' => t('Renders a menu as a block.'),
    'area' => [
      'id' => 'views_area_menu_block',
    ],
  ];

  return $data;
}
