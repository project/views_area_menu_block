# Views Area Menu Block

## Summary

This module provides a views area plugin that renders a menu as a block.

The menu block can be configured with the following options:

 * Block CSS class
 * Menu CSS class
 * Prefix path for all menu items
 * Suffix path for all menu items

## Requirements

Drupal core modules

- Views
- Views UI

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Usage

 * Create a menu
 * Create or edit a View
 * Add an area plugin to the header or footer of your View
 * Choose "Views Area Menu block"
 * Configure the settings for the "Views Area Menu block"
 * Save the View
