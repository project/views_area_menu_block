<?php

namespace Drupal\views_area_menu_block\Plugin\views\area;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\system\Entity\Menu;
use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Provides an area handler which renders an entity in a certain view mode.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_area_menu_block")
 */
class ViewsAreaMenuBlock extends AreaPluginBase {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $pluginId;

  /**
   * The plugin HTML DOM ID.
   *
   * @var string
   */
  public $pluginHtmlId;

  /**
   * The chosen menu ID.
   *
   * @var string
   */
  public $menuId;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['menu'] = ['default' => ''];
    $options['display_title'] = ['default' => FALSE];
    $options['menu_css_class'] = ['default' => ''];
    $options['block_css_class'] = ['default' => ''];
    $options['menu_prefix_path'] = ['default' => ''];
    $options['menu_suffix_path'] = ['default' => ''];
    $options['bypass_access'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    /** @var \Drupal\system\MenuInterface $type_menus */
    $type_menus = Menu::loadMultiple();
    $available_menus = [];
    foreach ($type_menus as $menu) {
      $available_menus[$menu->id()] = $menu->label();
    }
    $form['menu'] = [
      '#type' => 'select',
      '#options' => $available_menus,
      '#title' => $this->t('Menus'),
      '#default_value' => $this->options['menu'],
      '#description' => $this->t('Select the menu to render.'),
      '#required' => TRUE,
    ];

    $form['display_title'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display title'),
      '#default_value' => !empty($this->options['display_title']),
      '#description' => $this->t('Displays the menu title.'),
    ];

    $form['menu_css_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu CSS class'),
      '#default_value' => $this->options['menu_css_class'] ?? '',
      '#description' => $this->t('The class to provide on the menu element.'),
    ];

    $form['block_css_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block CSS class'),
      '#default_value' => $this->options['block_css_class'] ?? '',
      '#description' => $this->t('The class to provide on the block element.'),
    ];

    $form['menu_prefix_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix menu items with a path'),
      '#default_value' => $this->options['menu_prefix_path'] ?? '',
      '#description' => $this->t('Prefix to apply to the menu items.'),
    ];

    $form['menu_suffix_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix menu items with a path'),
      '#default_value' => $this->options['menu_suffix_path'] ?? '',
      '#description' => $this->t('Suffix to apply to the menu items.'),
    ];

    $form['bypass_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bypass access checks'),
      '#description' => $this->t('If enabled, access permissions for rendering the entity are not checked.'),
      '#default_value' => !empty($this->options['bypass_access']),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * We use system_menu_block as the plugin ID to enable menu theme suggestions.
   */
  public function render($empty = FALSE): array {
    $build = [];
    if (!$empty && !empty($this->options['menu'])) {
      [$menu, $type_menu] = $this->buildMenu();
      if ($menu !== NULL && $type_menu !== NULL) {
        // Underscore the block ID to ensure it is picked up by theme suggestions.
        $block_id = $this->pluginId . '_' . str_replace('-', '_', $this->menuId);
        $build = [
          '#theme' => 'block',
          '#attributes' => [
            'class' => [
              Html::cleanCssIdentifier($this->pluginId),
              Html::cleanCssIdentifier($this->pluginId . '-' . $this->menuId),
              Html::cleanCssIdentifier($this->options['block_css_class']) ?? '',
            ],
          ],
          '#configuration' => [
            'provider' => $this->pluginId,
            'label' => $type_menu->label(),
            'label_display' => !empty($this->options['display_title']),
          ],
          '#base_plugin_id' => 'system_menu_block',
          '#plugin_id' => 'system_menu_block:' . $this->menuId,
          '#id' => $block_id,
          'content' => $menu,
          '#block' => $type_menu,
          '#menu' => $this->menuId,
        ];
      }
    }
    return $build;
  }

  /**
   * Builds the menu for rendering in the block render array.
   */
  public function buildMenu(): array {
    $menu = NULL;
    $type_menu = Menu::load($this->options['menu']);
    if ($type_menu instanceof Menu) {
      $this->menuId = $type_menu->id();
      $menu_tree = \Drupal::menuTree();
      $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($this->menuId);
      $tree = $menu_tree->load($this->menuId, $parameters);
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];
      if (!empty($this->options['bypass_access'])) {
        $manipulators[] = ['callable' => 'menu.default_tree_manipulators:checkAccess'];
      }
      $tree = $menu_tree->transform($tree, $manipulators);
      $menu = $menu_tree->build($tree);
      if (!empty($menu['#items'])
        && (!empty($this->options['menu_prefix_path']) || !empty($this->options['menu_suffix_path']))
      ) {
        $this->applyMenuPrefixSuffix($menu);
      }
      $menu['#attributes']['class'][] = Html::cleanCssIdentifier($this->pluginId) . '-menu';
      if (!empty($this->options['menu_css_class'])) {
        $menu['#attributes']['class'][] = Html::cleanCssIdentifier($this->options['menu_css_class']);
      }
      $menu_theme = $menu['#theme'];
      $menu['#theme'] = [
        $menu_theme . '__' . $this->pluginId,
        $menu_theme,
      ];
    }
    return [$menu, $type_menu];
  }

  /**
   * Alters menu path.
   */
  private function applyMenuPrefixSuffix(&$menu): array {
    $menu_prefix_path = $this->options['menu_prefix_path'] ?? '';
    $menu_suffix_path = $this->options['menu_suffix_path'] ?? '';
    foreach ($menu['#items'] as $menu_item_key => $item) {
      /** @var \Drupal\Core\Url $url */
      $url = $item['url'];
      /** @var \Drupal\path_alias\AliasManager $path_alias_manager */
      $path_alias_manager = \Drupal::service('path_alias.manager');
      $alias = $path_alias_manager->getAliasByPath('/' . $url->getInternalPath());
      $path = NULL;
      if (!empty($menu_prefix_path)) {
        $path = $menu_prefix_path . $alias;
      }
      if (!empty($menu_suffix_path)) {
        $path = $menu_prefix_path . $alias . $menu_suffix_path;
      }
      if ($path !== NULL) {
        $new_url = Url::fromUri('internal:' . $path, $url->getOptions());
        $menu['#items'][$menu_item_key]['url'] = $new_url;
      }
    }
    return $menu;
  }

}
